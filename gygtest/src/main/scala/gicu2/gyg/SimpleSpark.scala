package gicu2.gyg

import org.apache.spark.{SparkConf, SparkContext}

object SimpleSpark {

  val appName = "192Test"
  val master = "local[*]"
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)

  def main(args: Array[String]): Unit = {

    val lines = sc.textFile("data.txt")
    val lineLengths = lines.map(s => s.length)
    lineLengths.persist()
    val totalLength = lineLengths.reduce((a, b) => a + b)
    println(totalLength)

  }

}
