object Solution {
  def solution(n_arg: Int): Int = {
    var n: Int = n_arg;
    var d: Array[Int] = new Array[Int](30);
    var l: Int = 0;
    while (n > 0) {
      d(l) = n % 2;
      n /= 2;
      l += 1;
    }
    val s = d.take(l).reverse.map(_.toString).reduce(_ + _)
    println(s)
    var p: Int = 1;
    while (p < 1 + l) {
      var ok: Boolean = true;
      var i: Int = 0;
      while (i < l - p) {
        if (d(i) != d(i + p)) {
          ok = false;
          i = l - p;
        }
        i += 1;
      }
      if (ok && i > p)
        return p;
      p += 1;
    }
    return -1;
  }

  def main(args: Array[String]): Unit = {
    println(solution(43))
    println(solution(955))
    println(solution(0xee45))
    println(solution(0xfeffef6))
  }

}
