
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.ml.tuning.{ParamGridBuilder, TrainValidationSplit}
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}

import sys.process._
import scala.language.postfixOps



object gygTest {

  val remoteFile = "https://www.dropbox.com/s/4j2toco09zj43i3/ds_dp_assessment.tar.gz?dl=0"
  val dataDir = "/tmp"
  val downName = "down.tar.gz"

  val appName = "gygTest"
  val master = "local[*]"
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)
  val sparkSession = SparkSession.builder.appName(appName).config("spark.master", master).getOrCreate()

  import java.sql.Timestamp

  case class TrainInput(Date: Timestamp, Keyword_ID: Long, Ad_group_ID: Long, Campaign_ID: Long, Account_ID: Long, Device_ID: Long,
                        Match_type_ID: Long, Revenue: Double, Clicks: Long, Conversions: Long)
  case class TestInput(Date:String,Keyword_ID:Long,Ad_group_ID:Long,Campaign_ID:Long,Account_ID:Long,Device_ID:Long,
                        Match_type_ID:Long)

  case class PerformanceTrain(Keyword_ID:Long,Ad_group_ID:Long,Campaign_ID:Long,Account_ID:Long,Device_ID:Long,
                              Match_type_ID:Long,AvrgRevenue:Double)
  case class PerformanceTest(Keyword_ID:Long,Ad_group_ID:Long,Campaign_ID:Long,Account_ID:Long,Device_ID:Long,
                             Match_type_ID:Long)



  def downloadData={
    val localFile = s"${dataDir}/$downName"
    s"wget -c -N $remoteFile -O $localFile" !!

    s"tar -xzf $localFile -C $dataDir" !!

  }

  def readCSV(name: String, delimiter: String = ",") = {
    val csvDF = sparkSession.read
      .option("header", "true")
      .option("delimiter", delimiter)
      .option("dateFormat", "yyyy-MM-dd")
      .option("inferSchema", "true")
      .option("nullValue", "null")
      .csv(name)
      .cache

    csvDF.printSchema
    println("Records: " + csvDF.count)
    csvDF.show

    csvDF
  }


  def main(args:Array[String]):Unit={

    import org.apache.spark.sql.functions._
    import sparkSession.implicits._

    downloadData

    // Read data

    val trainFile=dataDir + "/" + "train.csv"
    val testFile=dataDir + "/" + "prediction.csv"


    val inputTrainDS = readCSV(trainFile).as[TrainInput]
    val testDS = readCSV(testFile).as[PerformanceTest]

    // Average Revenue in train supposing Revenue independence on Date

    val trainDS0 = inputTrainDS
      .groupByKey(x=>(x.Keyword_ID,x.Ad_group_ID,x.Campaign_ID,x.Account_ID,x.Device_ID,x.Match_type_ID))
      .agg(sum($"Revenue").as[Double],sum($"Clicks").as[Long])
      .map(x=>PerformanceTrain(x._1._1,x._1._2,x._1._3,x._1._4,x._1._5,x._1._6,x._2/x._3))

    trainDS0.show

    // Validation

    // Set input features
    val assembler = new VectorAssembler()
      .setInputCols(Array("Keyword_ID", "Ad_group_ID", "Campaign_ID", "Account_ID", "Device_ID", "Match_type_ID"))
      .setOutputCol("features")

    val trainDS1 = assembler.transform(trainDS0).withColumnRenamed("AvrgRevenue", "label")
    val testDS1 = assembler.transform(testDS)

    val lr = new LinearRegression()
      .setMaxIter(10)
      .setFeaturesCol("features") // setting features column
    //      .setLabelCol("AvrgRevenue")

    // We use a ParamGridBuilder to construct a grid of parameters to search over.
    // TrainValidationSplit will try all combinations of values and determine best model using
    // the evaluator.
    val paramGrid = new ParamGridBuilder()
      .addGrid(lr.regParam, Array(0.1, 0.01))
      .addGrid(lr.fitIntercept)
      .addGrid(lr.elasticNetParam, Array(0.0, 0.5, 1.0))
      .build()

    // In this case the estimator is simply the linear regression.
    // A TrainValidationSplit requires an Estimator, a set of Estimator ParamMaps, and an Evaluator.
    val trainValidationSplit = new TrainValidationSplit()
      .setEstimator(lr)
      .setEvaluator(new RegressionEvaluator)
      .setEstimatorParamMaps(paramGrid)
      // 80% of the data will be used for training and the remaining 20% for validation.
      .setTrainRatio(0.8)
    // Evaluate up to 2 parameter settings in parallel
    //      .setParallelism(2)


    // Run train validation split, and choose the best set of parameters.
    val model = trainValidationSplit.fit(trainDS1)

    // Make predictions on test data. model is the model with combination of parameters
    // that performed best.
    val result = model.transform(testDS1)
    result.show()

    // Save results
    result.select("prediction")
      //      .withColumnRenamed("prediction", "loss")
      .coalesce(1)
      .write.format("csv")
      .option("header", "false")
      .mode(SaveMode.Overwrite)
      .save(dataDir + "/results.txt")

  }

}



