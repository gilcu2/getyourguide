package gicu2.gyg;


class SolutionMathStack {

    class MathStack {

        private final int StackSize = 1000;
        private final int Minimum = 0;
        private final int Maximum = 1 << 20;

        private int size = 0;
        private int[] data = new int[StackSize];

        private boolean condition(int value) {
            if (value >= Minimum && value < Maximum)
                return true;
            else
                return false;
        }

        int push(int value) {
            if (size < StackSize && condition(value)) {
                data[size] = value;
                size += 1;
                return value;
            } else
                return -1;

        }

        int pop() {
            if (size > 1) {
                size -= 1;
                return data[size - 1];
            } else
                return -1;
        }

        int dup() {
            if (size < StackSize && size > 0) {
                int value = data[size - 1];
                push(value);
                return value;
            } else return -1;

        }

        int add() {
            if (size > 1 && condition(data[size - 2] + data[size - 1])) {
                data[size - 2] += data[size - 1];
                size -= 1;
                return data[size - 1];
            } else
                return -1;
        }


        int subt() {
            if (size > 1 && condition(data[size - 1] - data[size - 2])) {
                data[size - 2] = data[size - 1] - data[size - 2];
                size -= 1;
                return data[size - 1];
            } else
                return -1;
        }


    }


    public int solution(String S) {

        MathStack machine = new MathStack();
        String[] operations = S.split(" ");

        int result = -1;
        for (String op : operations) {
            switch (op) {
                case "POP":
                    result = machine.pop();
                    break;
                case "DUP":
                    result = machine.dup();
                    break;
                case "+":
                    result = machine.add();
                    break;
                case "-":
                    result = machine.subt();
                    break;
                default:
                    int value;
                    try {
                        value = Integer.parseInt(op);
                    } catch (java.lang.NumberFormatException e) {
                        value = -1;
                    }

                    result = machine.push(value);
            }
            if (result == -1)
                break;
            ;

        }
        return result;
    }
}


class HelloWorldApp {


    public static void main(String[] args) {

        SolutionMathStack solution = new SolutionMathStack();
        System.out.println(solution.solution("13 DUP 4 POP 5 DUP + DUP + -"));
        System.out.println(solution.solution("pp"));

    }
}