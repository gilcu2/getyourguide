package gicu2.gyg

object SolutionCustomSort {

  def customSort(arrIni: Array[Int]): Array[Int] = {

    val arr = arrIni.tail

    def frecuency(value: Int): Int = {
      var frec = 0
      var pos = arr.indexOf(value)
      while (pos != -1) {
        frec += 1
        pos = arr.indexOf(value, pos + 1)
      }
      frec
    }

    val frecs = scala.collection.mutable.Map.empty[Int, Int]

    val withFrec: Array[(Int, Int)] = arr.map(x => {
      val frec = if (frecs.contains(x)) frecs(x) else {
        val frec1 = frecuency(x)
        frecs(x) = frec1
        frec1
      }
      (x, frec)
    })

    val sorted = withFrec.groupBy(_._2).map(x => {
      (x._1, x._2.map(_._1).sorted)
    }).toArray.sortBy(_._1)
      .flatMap(_._2)

    sorted

  }


  def main(args: Array[String]): Unit = {
    customSort(Array(5, 3, 1, 2, 2, 4)).foreach(println)
  }


}
