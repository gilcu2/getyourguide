package gicu2.gyg

object Solution {

  object Tree {
    def apply(values: Array[Int]): Tree = {
      val tree = new Tree()
      values.foreach(tree.insert(_))
      tree
    }
  }

  class Tree {

    case class Node(value: Int, var left: Node = null, var right: Node = null)

    var root: Node = null

    def insert(value: Int) {
      if (root == null) root = Node(value) else insert(value, root)
    }

    private def insert(value: Int, node: Node): Unit = {
      if (value < node.value) {
        if (node.left != null)
          insert(value, node.left)
        else {
          val newNode = Node(value)
          node.left = newNode
        }
      }
      else {
        if (node.right != null)
          insert(value, node.right)
        else {
          val newNode = Node(value)
          node.right = newNode
        }
      }

    }

  }

  def main(args: Array[String]): Unit = {
    Tree(Array(3, 8, 6))
  }


}
