import sys.process._
import scala.language.postfixOps


val remoteFile = "https://www.dropbox.com/s/4j2toco09zj43i3/ds_dp_assessment.tar.gz?dl=0"



val dataDir = "/tmp"



val downName = "down.tar.gz"



val localFile = s"${dataDir}/$downName"



s"wget -N $remoteFile -O $localFile" !!

/* ... new cell ... */

s"tar -xzf $localFile -C $dataDir" !!

/* ... new cell ... */

import org.apache.spark.sql.SparkSession



val sparkSession = SparkSession.builder.appName("GetYourGuideTest4Gilcu2").getOrCreate()

val trainFile=dataDir + "/" + "train.csv"

val testFile=dataDir + "/" + "prediction.csv"



//case class PerformanceTest(Date:String,Keyword_ID:Long,Ad_group_ID:Long,Campaign_ID:Long,Account_ID:Long,Device_ID:Long,Match_type_ID:Long)

//case class PerformanceTrain(Date:String,Keyword_ID:Long,Ad_group_ID:Long,Campaign_ID:Long,Account_ID:Long,Device_ID:Long,Match_type_ID:Long,

//                            Revenue:Double,Clicks:Long,Conversions:Long)





val trainDF = sparkSession.read

  .option("header", "true")

  .option("delimiter", ",")

  .option("dateFormat", "yyyy-MM-dd")

  .option("inferSchema", "true")

  .option("nullValue", "null")

  .csv(trainFile)



val testDF = sparkSession.read

  .option("header", "true")

  .option("delimiter", ",")

  .option("dateFormat", "yyyy-MM-dd")

  .option("inferSchema", "true")

  .option("nullValue", "null")

  .csv(testFile)

/* ... new cell ... */



trainDF.printSchema

trainDF.show



testDF.printSchema

testDF.show

/* ... new cell ... */

val selectedTrainDF=trainDF.select("Keyword_ID", "Ad_group_ID","Campaign_ID","Account_ID","Device_ID","Match_type_ID","Revenue","Clicks")

val selectedTestDF=testDF.select("Keyword_ID", "Ad_group_ID","Campaign_ID","Account_ID","Device_ID","Match_type_ID")



selectedTrainDF.printSchema

selectedTestDF.printSchema

/* ... new cell ... */

import org.apache.spark.sql.functions._



val groupedTrainDF = selectedTrainDF.groupBy("Keyword_ID", "Ad_group_ID","Campaign_ID","Account_ID","Device_ID","Match_type_ID")

val aggregatedTrainDF = groupedTrainDF.agg(sum("Revenue"),sum("Clicks"))



aggregatedTrainDF.show
