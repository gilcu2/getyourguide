package gicu2.gyg

object SolutionMinimumSum {

  def getMinimumUniqueSum(arrIni: Array[Int]): Int = {
    val arr = arrIni.tail

    def findUnique(value: Int, pos: Int): (Int, Int) = {
      var newValue = value + 1
      while (arr.indexOf(newValue) != -1)
        newValue += 1
      (newValue, pos)
    }

    val newArr = arr.zipWithIndex.map(x =>
      if (arr.indexOf(x._1, x._2 + 1) != -1)
        findUnique(x._1, x._2)
      else x)
      .map(_._1)
    newArr.sum
  }


  def main(args: Array[String]): Unit = {
    println(getMinimumUniqueSum(Array(3, 1, 2, 2)))
  }


}
